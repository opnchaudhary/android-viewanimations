package com.konnectcorp.devbtes;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {
	ImageView imageView;

	Button btnAlpha,btnTranslate,btnRotate,btnScale,btnMix;
	 AlphaAnimation alphaAnimation;
	 TranslateAnimation translateAnimation;
	 RotateAnimation rotateAnimation;
	 ScaleAnimation scaleAnimation;
	 AnimationSet setAnimation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		imageView = (ImageView) findViewById(R.id.imageView);
		btnAlpha = (Button) findViewById(R.id.btnAlpha);
		btnTranslate=(Button)findViewById(R.id.btnTranslate);
		btnRotate=(Button)findViewById(R.id.btnRotate);
		btnScale=(Button)findViewById(R.id.btnScale);
		btnMix=(Button)findViewById(R.id.btnMix);

		alphaAnimation = new AlphaAnimation(1, 0);
		alphaAnimation.setDuration(3000);

		translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0,
				Animation.RELATIVE_TO_PARENT, 1, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 100);
		translateAnimation.setDuration(3000);
		
		rotateAnimation=new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
		rotateAnimation.setDuration(3000);
		
		scaleAnimation=new ScaleAnimation(1,2, 1, 2);
		scaleAnimation.setDuration(3000);
		
		setAnimation=new AnimationSet(true);
		setAnimation.addAnimation(alphaAnimation);
		setAnimation.addAnimation(translateAnimation);
		setAnimation.addAnimation(scaleAnimation);


		btnAlpha.setOnClickListener(this);
		btnTranslate.setOnClickListener(this);
		btnRotate.setOnClickListener(this);
		btnScale.setOnClickListener(this);
		btnMix.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnAlpha:
			imageView.startAnimation(alphaAnimation);

			break;
		case R.id.btnTranslate:
			imageView.startAnimation(translateAnimation);
			break;
		case R.id.btnRotate:
			imageView.startAnimation(rotateAnimation);
			break;
		case R.id.btnScale:
			imageView.startAnimation(scaleAnimation);
		case R.id.btnMix:
			imageView.startAnimation(setAnimation);
		default:
			break;
		}

	}

}
